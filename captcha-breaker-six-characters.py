import logging
from datetime import datetime

import cv2
import flask
import numpy as np
import tensorflow as tf
from keras.applications.vgg16 import preprocess_input
from keras.engine.saving import load_model

app = flask.Flask(__name__)
model = None
graph = None


def decode(data):
    alphabet = "abcdefghijklmnopqrstuvwxyz0123456789"
    return alphabet[np.argmax(data)]


def model_setup():
    # load the pre-trained Keras model (here we are using a model
    # pre-trained on ImageNet and provided by Keras, but you can
    # substitute in your own networks just as easily)
    global model, graph

    model = load_model('./model-six-characters.h5')
    graph = tf.get_default_graph()  # Hack to work around TensorFlow/Flask incompatibilities - remove ASAP!

    logging.info('Finished loading and compiling model.')


@app.route("/predict", methods=["POST"])
def predict():
    # ensure an image was properly uploaded to our endpoint
    results = None
    if flask.request.method == "POST":
        if flask.request.files.get("image"):
            # read the image
            image_data = flask.request.files["image"].read()
            image_numpy_array = np.fromstring(image_data, np.uint8)
            captcha = cv2.imdecode(image_numpy_array, cv2.IMREAD_COLOR)
            captcha = cv2.cvtColor(captcha, cv2.COLOR_BGR2RGB).astype(np.float32)
            captcha = np.expand_dims(captcha, axis=0)
            captcha = preprocess_input(captcha)

            # classify the input image and then initialize the list
            # of predictions to return to the client
            with graph.as_default():  # Hack to work around TensorFlow/Flask incompatibilities - remove ASAP!
                preds = model.predict(captcha)
                results = [decode(char) for char in preds]
                results = ''.join(results)

                logging.basicConfig(filename='captchas-broken.log', level=logging.DEBUG)
                logging.info("{}    {}".format(datetime.now().strftime("%Y-%m-%d %H:%M:%S"), results))

    # return the data dictionary as a JSON response
    return results


if __name__ == '__main__':
    logging.getLogger().setLevel(logging.INFO)
    model_setup()
    app.run(port=6007)
