import tensorflow as tf
import numpy as np
from tensorflow.python.lib.io import file_io

TEMP_MODEL_FILE = 'model-six-characters.h5'


class GCSCheckpoint(tf.keras.callbacks.Callback):
    """Save the model to GCS after every epoch.
    # Arguments
        filepath: string, path to save the model file.
        monitor: quantity to monitor.
        verbose: verbosity mode, 0 or 1.
        save_best_only: if `save_best_only=True`,
            the latest best model according to
            the quantity monitored will not be overwritten.
        mode: one of {auto, min, max}.
            If `save_best_only=True`, the decision
            to overwrite the current save file is made
            based on either the maximization or the
            minimization of the monitored quantity. For `val_acc`,
            this should be `max`, for `val_loss` this should
            be `min`, etc. In `auto` mode, the direction is
            automatically inferred from the name of the monitored quantity.
        save_weights_only: if True, then only the model's weights will be
            saved (`model.save_weights(filepath)`), else the full model
            is saved (`model.save(filepath)`).
        period: Interval (number of epochs) between checkpoints.
    """

    def __init__(self, filepath, monitor='loss', verbose=0,
                 save_best_only=True, save_weights_only=False,
                 mode='auto', period=1):
        super(GCSCheckpoint, self).__init__()
        self.monitor = monitor
        self.verbose = verbose
        self.filepath = filepath
        self.save_best_only = save_best_only
        self.save_weights_only = save_weights_only
        self.period = period
        self.epochs_since_last_save = 0

        if mode not in ['auto', 'min', 'max']:
            tf.logging.warn('ModelCheckpoint mode %s is unknown, '
                            'fallback to auto mode.' % (mode),
                            RuntimeWarning)
            mode = 'auto'

        if mode == 'min':
            self.monitor_op = np.less
            self.best = np.Inf
        elif mode == 'max':
            self.monitor_op = np.greater
            self.best = -np.Inf
        else:
            if 'acc' in self.monitor or self.monitor.startswith('fmeasure'):
                self.monitor_op = np.greater
                self.best = -np.Inf
            else:
                self.monitor_op = np.less
                self.best = np.Inf

    def on_epoch_end(self, epoch, logs=None):
        logs = logs or {}
        self.epochs_since_last_save += 1
        if self.epochs_since_last_save >= self.period:
            self.epochs_since_last_save = 0
            if self.save_best_only:
                current = logs.get(self.monitor)
                if current is None:
                    tf.logging.warn('Can save best model only with %s available, '
                                    'skipping.' % (self.monitor), RuntimeWarning)
                else:
                    if self.monitor_op(current, self.best):
                        if self.verbose > 0:
                            tf.logging.info('\nEpoch %05d: %s improved from %0.5f to %0.5f,'
                                            ' saving model to %s'
                                            % (epoch + 1, self.monitor, self.best,
                                               current, TEMP_MODEL_FILE))
                        self.best = current
                        if self.save_weights_only:
                            self.model.save_weights(TEMP_MODEL_FILE, overwrite=True)
                            with file_io.FileIO(TEMP_MODEL_FILE, 'rb') as in_f:
                                with file_io.FileIO(self.filepath, 'w+') as out_f:
                                    out_f.write(in_f.read())

                        else:
                            self.model.save(TEMP_MODEL_FILE, overwrite=True)
                            with file_io.FileIO(TEMP_MODEL_FILE, 'rb') as in_f:
                                with file_io.FileIO(self.filepath, 'w+') as out_f:
                                    out_f.write(in_f.read())

                    else:
                        if self.verbose > 0:
                            tf.logging.info('\nEpoch %05d: %s did not improve from %0.5f' %
                                            (epoch + 1, self.monitor, self.best))
            else:
                if self.verbose > 0:
                    tf.logging.info('\nEpoch %05d: saving model to %s' % (epoch + 1, TEMP_MODEL_FILE))
                if self.save_weights_only:
                    self.model.save_weights(TEMP_MODEL_FILE, overwrite=True)
                    with file_io.FileIO(TEMP_MODEL_FILE, 'rb') as in_f:
                        with file_io.FileIO(self.filepath, 'w+') as out_f:
                            out_f.write(in_f.read())
                else:
                    self.model.save(TEMP_MODEL_FILE, overwrite=True)
                    with file_io.FileIO(TEMP_MODEL_FILE, 'rb') as in_f:
                        with file_io.FileIO(self.filepath, 'w+') as out_f:
                            out_f.write(in_f.read())
