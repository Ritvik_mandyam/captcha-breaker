from os import path

import cv2
import numpy as np
from keras import Model, Input
from keras.applications.vgg16 import VGG16, preprocess_input
from keras.callbacks import TensorBoard, ReduceLROnPlateau, EarlyStopping, ModelCheckpoint
from keras.engine.saving import load_model
from keras.layers import Dense
from keras.optimizers import Adadelta
from keras.utils import to_categorical
from six.moves import urllib


def main():
    if not path.exists('./model-six-characters.h5'):
        print('Model doesn\'t exist, starting fresh training...')
        main_input = Input((80, 215, 3))

        convnet = VGG16(include_top=False, weights=None, pooling='max')(main_input)

        classifier = Dense(1024, activation='relu')(convnet)
        classifier = Dense(512, activation='relu')(classifier)

        char_1 = Dense(36, activation='softmax', name='char_1')(classifier)
        char_2 = Dense(36, activation='softmax', name='char_2')(classifier)
        char_3 = Dense(36, activation='softmax', name='char_3')(classifier)
        char_4 = Dense(36, activation='softmax', name='char_4')(classifier)
        char_5 = Dense(36, activation='softmax', name='char_5')(classifier)
        char_6 = Dense(36, activation='softmax', name='char_6')(classifier)

        output = [char_1, char_2, char_3, char_4, char_5, char_6]

        model = Model(inputs=main_input, outputs=output)

        adadelta = Adadelta(lr=0.005)
        model.compile(optimizer=adadelta, loss='categorical_crossentropy', metrics=['acc'])

    else:
        print('Found existing model, resuming training...')
        model = load_model('./model-six-characters.h5')

    tbCallBack = TensorBoard(write_grads=True, write_graph=True, write_images=True, update_freq=200)
    learning_rate_reduction = ReduceLROnPlateau(monitor='loss', patience=2, verbose=1, factor=0.4, min_lr=0.000001)
    early_stopping = EarlyStopping(monitor='loss', min_delta=0, patience=3, verbose=1, mode='auto')
    modelCheckpointCallback = ModelCheckpoint('model-six-characters.h5', monitor='loss', verbose=1,
                                              save_best_only=True)

    model.fit_generator(captcha_generator(), epochs=100, steps_per_epoch=2000,
                        callbacks=[tbCallBack, learning_rate_reduction, early_stopping, modelCheckpointCallback],
                        use_multiprocessing=True, workers=4, max_queue_size=8)


def captcha_generator():
    while True:
        # Replace with http://data1.impelcrm.in... in dev mode
        base_url = 'http://i1947.impelcrm.in:82/securimage/securimage_show.php?ctype=4l'
        with urllib.request.urlopen(base_url) as response:
            image_data = response.read()
            headers = response.info()._headers
            image_numpy_array = np.fromstring(image_data, np.uint8)
            captcha = cv2.imdecode(image_numpy_array, cv2.IMREAD_COLOR)
            captcha = cv2.cvtColor(captcha, cv2.COLOR_BGR2RGB)
            captcha = np.expand_dims(captcha, axis=0).astype(np.float32)
            captcha = np.array(preprocess_input(captcha))
            captcha_code = encode(headers[8][1].lower())
            yield captcha, captcha_code


def encode(data):
    alphabet = "abcdefghijklmnopqrstuvwxyz0123456789"
    # Creates a dict, that maps to every char of alphabet an unique int based on position
    char_to_int = dict((c, i) for i, c in enumerate(alphabet))
    encoded_data = [np.expand_dims(to_categorical(char_to_int[char], num_classes=36), axis=0) for char in data]
    return encoded_data


if __name__ == '__main__':
    main()
    # convert_to_onehot('abcde')
